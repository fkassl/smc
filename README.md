# smc
SMC - Контроллер управления тремя шаговомы двигателями типа - ```ПБМГ-200-265 ф``` на Arduino.

Разработка прошивки микроконтроллера ведётся на [github.com](https://github.com/rusakovprz/smc)

Разработка аппаратной части ведётся на [bitbucket.org](https://bitbucket.org/fkassl/smc)

[Документация](https://bitbucket.org/fkassl/smc/wiki/Home)
